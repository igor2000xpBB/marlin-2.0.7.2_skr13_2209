/**
 * Marlin 3D Printer Firmware
 * Copyright (c) 2020 MarlinFirmware [https://github.com/MarlinFirmware/Marlin]
 *
 * Based on Sprinter and grbl.
 * Copyright (c) 2011 Camiel Gubbels / Erik van der Zalm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#pragma once

// R25 = 100 kOhm, beta25 = 3950 K, 4.7 kOhm pull-up, QU-BD silicone bed QWG-104F-3950 thermistor
const temp_entry_t temptable_11[] PROGMEM = {
  /*
  { OV(   1), 938 },
  { OV(  31), 314 },
  { OV(  41), 290 },
  { OV(  51), 272 },
  { OV(  61), 258 },
  { OV(  71), 247 },
  { OV(  81), 237 },
  { OV(  91), 229 },
  { OV( 101), 221 },
  { OV( 111), 215 },
  { OV( 121), 209 },
  { OV( 131), 204 },
  { OV( 141), 199 },
  { OV( 151), 195 },
  { OV( 161), 190 },
  { OV( 171), 187 },
  { OV( 181), 183 },
  { OV( 191), 179 },
  { OV( 201), 176 },
  { OV( 221), 170 },
  { OV( 241), 165 },
  { OV( 261), 160 },
  { OV( 281), 155 },
  { OV( 301), 150 },
  { OV( 331), 144 },
  { OV( 361), 139 },
  { OV( 391), 133 },
  { OV( 421), 128 },
  { OV( 451), 123 },
  { OV( 491), 117 },
  { OV( 531), 111 },
  { OV( 571), 105 },
  { OV( 611), 100 },
  { OV( 641),  95 },
  { OV( 681),  90 },
  { OV( 711),  85 },
  { OV( 751),  79 },
  { OV( 791),  72 },
  { OV( 811),  69 },
  { OV( 831),  65 },
  { OV( 871),  57 },
  { OV( 881),  55 },
  { OV( 901),  51 },
  { OV( 921),  45 },
  { OV( 941),  39 },
  { OV( 971),  28 },
  { OV( 981),  23 },
  { OV( 991),  17 },
  { OV(1001),   9 },
  { OV(1021), -27 }
*/
{ OV(   1), 938 },
{ OV( 10), 350 },
{ OV( 12), 340 },
{ OV( 13), 330 },
{ OV( 15), 320 },
{ OV( 17), 310 },
{ OV( 20), 300 },
{ OV( 23), 290 },
{ OV( 27), 280 },
{ OV( 32), 270 },
{ OV( 35), 265 },
{ OV( 37), 260 },
{ OV( 41), 255 },
{ OV( 44), 250 },
{ OV( 48), 245 },
{ OV( 53), 240 },
{ OV( 58), 235 },
{ OV( 63), 230 },
{ OV( 69), 225 },
{ OV( 76), 220 },
{ OV( 83), 215 },
{ OV( 91), 210 },
{ OV( 100), 205 },
{ OV( 110), 200 },
{ OV( 121), 195 },
{ OV( 133), 190 },
{ OV( 146), 185 },
{ OV( 161), 180 },
{ OV( 176), 175 },
{ OV( 192), 170 },
{ OV( 210), 165 },
{ OV( 229), 160 },
{ OV( 250), 155 },
{ OV( 273), 150 },
{ OV( 298), 145 },
{ OV( 325), 140 },
{ OV( 354), 135 },
{ OV( 384), 130 },
{ OV( 417), 125 },
{ OV( 451), 120 },
{ OV( 486), 115 },
{ OV( 523), 110 },
{ OV( 561), 105 },
{ OV( 599), 100 },
{ OV( 617), 98 },
{ OV( 653), 93 },
{ OV( 689), 88 },
{ OV( 724), 83 },
{ OV( 758), 78 },
{ OV( 790), 73 },
{ OV( 820), 68 },
{ OV( 848), 63 },
{ OV( 860), 60 },
{ OV( 883), 55 },
{ OV( 905), 50 },
{ OV( 924), 45 },
{ OV( 940), 40 },
{ OV( 955), 35 },
{ OV( 967), 30 },
{ OV( 978), 25 },
{ OV( 985), 23 },
{ OV( 993), 18 },
{ OV( 999), 13 },
{ OV( 1005), 8 },
{ OV( 1009), 3 },
{ OV( 1013), -2 },
{ OV( 1016), -7 },
{ OV( 1018), -12 },
{ OV( 1019), -17 },
{ OV( 1021), -22 },
{ OV( 1022), -27 }
};
